<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


/**
 * Class AccountController
 * @package AppBundle\Controller
 */
class AccountController extends Controller
{
    /**
     * @Route("/account", name="account")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request)
    {
        $account = $this->getDoctrine()->getRepository('AppBundle:User')->findBy(array('id' => $this->get('security.token_storage')->getToken()->getUser()->getId()));

        return $this->render('account/index.html.twig', array(
            'account' => $account
        ));
    }

    /**
     * @Route("/account/edit", name="account_edit")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request)
    {

        $account = $this->get('security.token_storage')->getToken()->getUser();

        $account->setUsername($account->getUsername());
        $account->setEmail($account->getEmail());
        $account->setIsActive($account->getIsActive());

        $form = $this->createForm(UserType::class, $account);

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $account->setUsername($form['username']->getData());
            $password = $this->get('security.password_encoder')
                ->encodePassword($account, $form['password']->getData());
            $account->setPassword($password);
            $account->setEmail($form['email']->getData());

            $em->flush();

            $this->addFlash(
                'notice',
                'account edited'
            );

            return $this->redirectToRoute('account');
        }

        return $this->render('account/edit.html.twig', array(
            '$account' => $account,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/account/edit/picture", name="account_edit_picture")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editPictureAction(Request $request)
    {
        return $this->render('account/edit_picture.html.twig', [
            'uploadUri' => $this->generateUrl('account_upload_picture', [], UrlGeneratorInterface::ABSOLUTE_URL)
        ]);

    }

    /**
     * @Route("/account/upload/picture", name="account_upload_picture")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function uploadPictureAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $file = $request->files->get('file');

        // Generate a unique name for the file before saving it
        $fileName = md5(uniqid()).$file->getClientOriginalName();

        // Move the file to the directory where brochures are stored
        $file->move(
            $this->getParameter('picture_directory'),
            $fileName
        );

        $user->setPicture($fileName);
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new JsonResponse([], 200);

    }

}
