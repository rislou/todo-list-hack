# Symfony 3 TODO List

Just another TODO app to show some basic and new features on Symfony 3.

## Installation

	composer install
	
## Run the server
   
    php bin/console server:run

Check the application: http://localhost:8000/

## Commands

   
### Create empty database todolist
    
    create database todolist
    
### Update Database Schema with dump

    mysql -u youruser -p todolist < todolist_dump.sql

